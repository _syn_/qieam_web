import { Component, OnInit } from '@angular/core';
import {Friend} from'../friend';
import {FriendService} from '../friend.service';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent implements OnInit {
	friends:Friend[] = [];
	urlAvatar = 'https://i.pravatar.cc/150?u=';
    constructor(private friendService:FriendService) { }

  ngOnInit(): void {
  }
    myEncode(toEncode: string): string {
    return btoa(toEncode);
  }
  
  viewFriends():void{
  
	console.log("hui-ouze");
	this.friendService.getFriends().subscribe(res => this.friends=res.friends);
  
  }

}
